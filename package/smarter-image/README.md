# SMARTER K3S server

This repository provides a container image that allows k3s server to run wihtout installing. 
The container persists data on /var/lib/rancher/k3s and the script below (also present on the repo) stores that on a volume set locally. 
The image can be run wihtout setting the volume but the server data and configuration will be lost when the container stos.
The script [k3s-start.sh.template script](k3s-start.sh.template) also copies the token and credentials to access the server, token.${SERVERNAME} and kube.${SERVERNAME}.config.
The use should donwload the template [k3s-start.sh.template script](k3s-start.sh.template) and edit the variables and change it k3s-start-<SERVERNAME>.sh so the SMARTER k3s server can be started (or restarted) with the same configurations.

The main characteristic of this image is a script that removes the node-password when a node is removed.

A few tips: 
* The HOSTIP is the IP that the nodes will connect to, if using a VM in AWS or NATing, this should be the external ip.
* Do not NAT the port. It confuses the nodes. If you need to change the port change it on the script.
* The script allows multiple copies to run, just use different SERVERNAMEs and HOSTPORs.
