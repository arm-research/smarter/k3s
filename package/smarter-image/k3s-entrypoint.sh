#!/bin/sh

shift 1
echo "Starting k3s with parameters $*"

echo "Starting the password remover"
nohup /usr/bin/remove-password-deleted-nodes.sh > /tmp/log-remove.txt 2>&1 &

echo "Starting k3s"
exec /bin/k3s server $*
